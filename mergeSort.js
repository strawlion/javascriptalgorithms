// MergeSort written in javascript

function mergeSort(arrayToSort){
    // Base Case
    if (arrayToSort.length === 1) {
        return;
    }
    
    var halfOne = [];
    var halfTwo = [];
	
    // Split the array into two halves
    for (var i = 0; i < Math.floor(arrayToSort.length/2); i++) {
        halfOne[i] = arrayToSort[i];
    }
	
    for (var j = Math.floor(arrayToSort.length/2); j < arrayToSort.length; j++) {
        halfTwo[j - Math.floor(arrayToSort.length/2)] = arrayToSort[j];
    }
    
    mergeSort(halfOne);
    mergeSort(halfTwo);
    
    merge(halfOne, halfTwo, arrayToSort);
    
}

// Perform the actual sort
function merge(halfOne, halfTwo, originalArray) {

    var halfOnePointer = 0;
    var halfTwoPointer = 0;
    var originalArrayPointer = 0;    
    
	
	// Iterate through and sort the elements in ascending order
	while (halfOnePointer < halfOne.length && halfTwoPointer < halfTwo.length) {
		var elementFromHalfOne = halfOne[halfOnePointer];
		var elementFromHalfTwo = halfTwo[halfTwoPointer];
		
		
		if (elementFromHalfOne < elementFromHalfTwo) {
            originalArray[originalArrayPointer] = elementFromHalfOne;  
            halfOnePointer++;
        }
        else 
        {
            originalArray[originalArrayPointer] = elementFromHalfTwo;    
            halfTwoPointer++
        }
		
		originalArrayPointer++;
	}
	
	var isHalfOnePointerOutOfBounds = (halfOnePointer === halfOne.length);
	if (isHalfOnePointerOutOfBounds) {
		copyValuesFromArrayToArray(halfTwo, halfTwoPointer, originalArray, originalArrayPointer);
	} 
	// Else halfTwoPointer is out of bounds
	else {
		copyValuesFromArrayToArray(halfOne, halfOnePointer, originalArray, originalArrayPointer);
	}

    
    return originalArray;
    
}

function copyValuesFromArrayToArray(arrayToCopyFrom, arrayToCopyFromPointer, arrayToCopyTo, arrayToCopyToPointer) {

	// While array to copy from pointer is not out of bounds
	while (arrayToCopyFromPointer !== arrayToCopyFrom.length) {
			elementToCopy = arrayToCopyFrom[arrayToCopyFromPointer];
			arrayToCopyTo[arrayToCopyToPointer] = elementToCopy;    
            arrayToCopyFromPointer++
			arrayToCopyToPointer++;
	}
	
	return arrayToCopyTo;
}